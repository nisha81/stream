<!DOCTYPE html>
<head>
<meta charset="utf-8"/>
<link rel="stylesheet" href="http://sign-guild.de/wp-content/themes/yoo_master2_wp/streams/css/style.css" type="text/css"/>
<script type="text/javascript">

var channels = new Array( 'hndnisha', 'kacaos1990', 'monstercat'); // Add channel names here rotterdam08
var offstreams = "0";
channels.sort();
jQuery(document).ready(function() {

	var updateStreams = function() {
		jQuery("#streams").empty();
		jQuery("#streamsoff").empty();
		jQuery.each(channels, function(index, value) {
			var url = 'https://api.twitch.tv/kraken/streams/'+ this +'?callback=?';

			jQuery.getJSON(url, function(s){

			function decodewow()
			{
				if (s.stream.game == "World of Warcraft: Warlords of Draenor"){
					return "World of Warcraft";
				}

				else{
					return s.stream.game;
				}
			}
			if (s.stream != null){
				// alert(s.stream.game);
				var rows = '';
				var row = '<a href="http://sign-guild.de/streams/" target="_blank"><div class="streamlist">';
				if (s.stream.channel.logo == null){
					row += '<img class="gameicon2" src="http://sign-guild.de/wp-content/themes/yoo_master2_wp/streams/images/signstream.png">';
				}
				else
				{
					row += '<img class="logoicon" src="' + s.stream.channel.logo + '">';
				}
				if (s.stream.game == "Hearthstone: Heroes of Warcraft")
				{
					row += '<img class="gameicon2" src="http://sign-guild.de/wp-content/themes/yoo_master2_wp/streams/icon/hstone.png">'; 
				}
				else if (s.stream.game == "StarCraft II: Heart of the Swarm")
				{
					row += '<img class="gameicon2" src="http://sign-guild.de/wp-content/themes/yoo_master2_wp/streams/icon/sc2.png">';
				}
				else if (s.stream.game == "Diablo III: Reaper of Souls")
				{
					row += '<img class="gameicon2" src="http://sign-guild.de/wp-content/themes/yoo_master2_wp/streams/icon/d3.png">';
				}
				else if (s.stream.game == "World of Warcraft: Warlords of Draenor")
				{
					row += '<img class="gameicon2" src="http://sign-guild.de/wp-content/themes/yoo_master2_wp/streams/icon/wow.png">';
				}
				else if (s.stream.game == "League of Legends")
				{
					row += '<img class="gameicon2" src="http://sign-guild.de/wp-content/themes/yoo_master2_wp/streams/icon/lol.png">';
				}
				else if (s.stream.game == "WildStar")
				{
					row += '<img class="gameicon2" src="http://sign-guild.de/wp-content/themes/yoo_master2_wp/streams/icon/ws.png">';
				}
				else if (s.stream.game == "Music")
				{
					row += '<img class="gameicon2" src="http://sign-guild.de/wp-content/themes/yoo_master2_wp/streams/images/musik.png">';
				}
				row += '<div class="streamer">';
				row += s.stream.channel.name;
				row += '</div>';
				row += '<div class="arrow"><img src="http://sign-guild.de/wp-content/themes/yoo_master2_wp/streams/images/arrow.png"></div></br>';
				row += '<div class="desc"><b>Game:</b> ' + decodewow(s.stream.game) + '<br/>';
				row += s.stream.channel.status.substring(0, 30); + '</br>';
				row += '</div>';
				row += '</div></div></a>';
				rows += row;

				jQuery("#streams2").append(rows);
				}
				if (s.stream == null){
					offstreams++;
					if (offstreams >= 3){
						var rows = '';
						var row = 'Es ist momentan kein Stream online!';
						rows += row;

						jQuery("#offst").append(rows);
					}
				}
			});
		});
		}		
	jQuery("#btnRefresh").click(function(){updateStreams()});
	updateStreams();
});
</script>
</head>
<body>
<div id="streams2"></div>
<div id="offst"></div>
<!-- <div class="stbutton"><input id="btnRefresh" type="button" value="Aktualisieren"></div> -->
</body>
</html>