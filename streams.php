<!DOCTYPE html><head>
<link rel="stylesheet" href="http://sign-guild.de/wp-content/themes/yoo_master2_wp/streams/css/style.css" type="text/css"/>
<meta charset="utf-8"/>
<script src="https://ttv-api.s3.amazonaws.com/twitch.min.js"></script>
<script>
  Twitch.init({clientId: 'ibgi0jycf73wqfwn4cjs1zhcv5utn2g'}, function(error, status) {
  if (error) {
    // error encountered while loading
    console.log(error);
  }
  // the sdk is now loaded
  if (status.authenticated) {
    // user is currently logged in
  }
});
</script>
<script type="text/javascript">
var name1 = 'name';
var game1 = 'game';
var titel1 = 'titel';
var on1 = 'no';

var name2 = 'name';
var game2 = 'game';
var titel2 = 'titel';
var on2 = 'no';

var name3 = 'name';
var game3 = 'game';
var titel3 = 'titel';
var on3 = 'no';

var channels = new Array( 'monstercat', 'hndnisha', 'kacaos1990', 'monstercat' ); // Add channel names here monstercat

$(document).ready(function() {

	var updateStreams2 = function() {
		$("#streams").empty();
		$.each(channels, function(index, value) {
			var url = 'https://api.twitch.tv/kraken/streams/'+ this +'?callback=?';

			$.getJSON(url, function(s){
				// alert(s.stream);

				function gameicon(){				
					if (s.stream.game == "Hearthstone: Heroes of Warcraft")
					{
						return '<img class="gameicon" src="http://sign-guild.de/wp-content/themes/yoo_master2_wp/streams/icon/hstone.png">'; 
					}
					if (s.stream.game == "StarCraft II: Heart of the Swarm")
					{
						return '<img class="gameicon" src="http://sign-guild.de/wp-content/themes/yoo_master2_wp/streams/icon/sc2.png">';
					}
					if (s.stream.game == "Diablo III: Reaper of Souls")
					{
						return '<img class="gameicon" src="http://sign-guild.de/wp-content/themes/yoo_master2_wp/streams/icon/d3.png">';
					}
					if (s.stream.game == "World of Warcraft: Warlords of Draenor")
					{
						return '<img class="gameicon" src="http://sign-guild.de/wp-content/themes/yoo_master2_wp/streams/icon/wow.png">';
					}
					if (s.stream.game == "League of Legends")
					{
						return '<img class="gameicon" src="http://sign-guild.de/wp-content/themes/yoo_master2_wp/streams/icon/lol.png">';
					}
					if (s.stream.game == "WildStar")
					{
						return '<img class="gameicon" src="http://sign-guild.de/wp-content/themes/yoo_master2_wp/streams/icon/ws.png">';
					}
					if (s.stream.game == "Music")
					{
						return '<img class="gameicon" style="width: 50px; height:50px;" src="http://sign-guild.de/wp-content/themes/yoo_master2_wp/streams/icon/musik.png">';
					}
				}

				function decodewow()
				{
					if (s.stream.game == "World of Warcraft: Warlords of Draenor"){
						return "World of Warcraft";
					}

					else{
						return s.stream.game;
					}
				}
				if (s.stream != null){

					var sub2 = '<img class="follower" src="http://sign-guild.de/wp-content/themes/yoo_master2_wp/streams/images/chansub.png"></img>';
					var viewers2 = '<img class="viewers" src="http://sign-guild.de/wp-content/themes/yoo_master2_wp/streams/images/viewers.png"></img>';
					var rows = '';
					if (value == 'hndnisha') {
						var row ='<div class="stream-holder2"><a id="myLink" onclick="stream1();">';
						name1 = s.stream.channel.display_name;
						game1 = gameicon(s.stream.game)
						titel1 = s.stream.channel.status.substring(0, 75);
						on1 = 'yes';
					}
					if (value == 'kacaos1990') {
						var row = '<div class="stream-holder2"><a id="myLink" onclick="stream2();">';
						name2 = s.stream.channel.display_name;
						game2 = gameicon(s.stream.game)
						titel2 = s.stream.channel.status.substring(0, 75);
						on2 = 'yes';
					}
					if (value == 'monstercat') {
						var row = '<div class="stream-holder2"><a id="myLink" onclick="stream3();">';
						name3 = s.stream.channel.display_name;
						game3 = gameicon(s.stream.game)
						titel3 = s.stream.channel.status.substring(0, 75);
						on3 = 'yes';
					}
					row +=  '<div class="stream"><div class="stream_window"><img class="onlinebadge" src="http://sign-guild.de/wp-content/themes/yoo_master2_wp/streams/images/onlinebadge.png" width="90" height="76"><img class="thumb_bg "src="' + s.stream.preview.medium + '" width="135" height="115"/></img></div><div class="stream_title_on">'+ s.stream.channel.display_name + '</div><div class="gametit"><b>Game:</b>  '+ decodewow(s.stream.game) + '</div>';
		 			row += '<div class="sttitle"><b>Titel:  </b>' + s.stream.channel.status.substring(0, 80) + '</div>';
		 			row += '';
		 			row += '<div class="follow">' + sub2 + s.stream.channel.followers +'</div><div class ="viewer"> '+ viewers2 + s.stream.viewers + '</div></div></a>';
		 			row += '</div></a></div>';
					rows += row;
					$("#streams").append(rows);
				}
				if (s.stream == null){
					var rows = '';
					if (value == 'hndnisha') {
						var row ='<div class="stream-holder2"><a id="myLink" onclick="stream1();">';
					}
					if (value == 'kacaos1990') {
						var row = '<div class="stream-holder2"><a id="myLink" onclick="stream2();">';
					}
					if (value == 'monstercat') {
						var row = '<div class="stream-holder2"><a id="myLink" onclick="stream3();">';
					}
					row +=  '<div class="stream"><img class="onlinebadge" src="http://sign-guild.de/wp-content/themes/yoo_master2_wp/streams/images/offlinebadge.png" width="90" height="76"><img class="thumb_bg2" src="http://sign-guild.de/wp-content/themes/yoo_master2_wp/streams/images/twitch2.jpg" width="135" height="115"/><div class="stream_title_off">'+ value +'</div><div class="gametit"><b>Game:</b> --- </div>';
		 			row += '<div class="sttitle"><b>Titel:</b> --- </div>';
		 			row += '</div></a></div>';

					rows += row;
					
					$("#streamsoff").append(rows);
				}
			});
		});
		}		
	updateStreams2();
});

	function stream1()
	{
		var stream = ('<div class="stream_player"><object type="application/x-shockwave-flash" height="360" width="640" id="live_embed_player_flash" data="http://de.twitch.tv/widgets/live_embed_player.swf?channel=hndnisha" bgcolor="#000000"><param name="allowFullScreen" value="true" /><param name="allowScriptAccess" value="always" /><param name="allowNetworking" value="all" /><param name="movie" value="http://de.twitch.tv/widgets/live_embed_player.swf" /><param name="flashvars" value="hostname=de.twitch.tv&channel=hndnisha&auto_play=true&start_volume=50" /></object></div>');
		document.getElementById("stream").innerHTML=("")
		document.getElementById("stream").innerHTML=(stream)
		document.getElementById("stream_info").innerHTML=("")
		if (on1 == 'yes'){
			document.getElementById("stream_info").innerHTML=('<div class="info"><div class="gameicon">' + game1 + '</div><div class="stream_info"><span class="stream_name">' + name1 + '</span></br><span class="stream_tit">' + titel1 + '</span></div></div>')
		}
	}
	function stream2()
	{
		var stream = ('<div class="stream_player"><object type="application/x-shockwave-flash" height="360" width="640" id="live_embed_player_flash" data="http://de.twitch.tv/widgets/live_embed_player.swf?channel=kacaos1990" bgcolor="#000000"><param name="allowFullScreen" value="true" /><param name="allowScriptAccess" value="always" /><param name="allowNetworking" value="all" /><param name="movie" value="http://de.twitch.tv/widgets/live_embed_player.swf" /><param name="flashvars" value="hostname=de.twitch.tv&channel=kacaos1990&auto_play=true&start_volume=50" /></object></div>');
		document.getElementById("stream").innerHTML=("")
		document.getElementById("stream").innerHTML=(stream)
		document.getElementById("stream_info").innerHTML=("")
		if (on2 == 'yes'){
			document.getElementById("stream_info").innerHTML=('<div class="info"><div class="gameicon">' + game2 + '</div><div class="stream_info"><span class="stream_name">' + name2 + '</span></br><span class="stream_tit">' + titel2 + '</span></div></div>')
		}
	}
	function stream3()
	{
		var stream = ('<div class="stream_player"><object type="application/x-shockwave-flash" height="360" width="640" id="live_embed_player_flash" data="http://de.twitch.tv/widgets/live_embed_player.swf?channel=monstercat" bgcolor="#000000"><param name="allowFullScreen" value="true" /><param name="allowScriptAccess" value="always" /><param name="allowNetworking" value="all" /><param name="movie" value="http://de.twitch.tv/widgets/live_embed_player.swf" /><param name="flashvars" value="hostname=de.twitch.tv&channel=monstercat&auto_play=true&start_volume=50" /></object></div>');
		document.getElementById("stream").innerHTML=("")
		document.getElementById("stream").innerHTML=(stream)
		document.getElementById("stream_info").innerHTML=("")
		if (on3 == 'yes'){
			document.getElementById("stream_info").innerHTML=('<div class="info"><div class="gameicon">' + game3 + '</div><div class="stream_info"><span class="stream_name">' + name3 + '</span></br><span class="stream_tit">' + titel3 + '</span></div></div>')
		}
	}
</script>
</head>

<div class="description">
	Livestreams sind ein wichtiger Bestandteil der heutigen Online-Gaming-Szene. 
	Schaut doch von unseren WoW-Spielern über die Schulter, auch wenn sie gerade etwas anderes spielen, und genießt unsere Streams</br></div>
<div class="stream_mar">
	<div id="stream_info"></div>
	<div id="stream"></div>
</div>
<div id="streams"></div>
<div id="streamsoff"></div>
<div style="min-height: 350px;"></div>
</html>